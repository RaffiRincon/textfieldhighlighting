//
//  ViewController.m
//  TextFieldHighlighting
//
//  Created by Rafael Rincon on 7/27/16.
//  Copyright © 2016 CornerGames. All rights reserved.
//

#import "ViewController.h"
#import "HighlightableTextView.h"

@interface ViewController ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet HighlightableTextView *textView;

@end

@implementation ViewController


@end
