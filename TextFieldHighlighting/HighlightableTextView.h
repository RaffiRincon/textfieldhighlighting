//
//  HighlightableTextView.h
//  TextFieldHighlighting
//
//  Created by Rafael Rincon on 7/29/16.
//  Copyright © 2016 CornerGames. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HighlightableTextView : UITextView

@end
