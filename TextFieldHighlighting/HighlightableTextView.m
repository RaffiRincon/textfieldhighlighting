//
//  HighlightableTextView.m
//  TextFieldHighlighting
//
//  Created by Rafael Rincon on 7/29/16.
//  Copyright © 2016 CornerGames. All rights reserved.
//

#import "HighlightableTextView.h"

@interface HighlightableTextView ()<UIGestureRecognizerDelegate>

@property NSUInteger startIndex;
@property NSUInteger endIndex;
@property NSUInteger numLines;
@property NSMutableAttributedString *mutableAttributedText;
@property UILongPressGestureRecognizer *longPressRecognizer;
@property CGPoint contentOffsetAtStartLongPress;

@end

@implementation HighlightableTextView

-(void)awakeFromNib {
	[super awakeFromNib];
	self.editable = false;
	self.selectable = false;
	
	self.longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPressWithRecognizer:)];
	self.longPressRecognizer.delegate = self;
	
	[self addGestureRecognizer:self.longPressRecognizer];
}

-(void)layoutSubviews {
	[self calculateAndPrintNumLines];
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
	return true;
}

-(void)calculateAndPrintNumLines {
	NSRange charRange = [self.layoutManager glyphRangeForTextContainer:self.textContainer];
	
	NSUInteger index;
	NSRange lineRange;
	
	for (self.numLines = 0, index = 0; index < charRange.length; self.numLines++) {
		(void)[self.layoutManager lineFragmentRectForGlyphAtIndex:index effectiveRange:&lineRange];
		index = NSMaxRange(lineRange);
		
	}
}

-(void)onLongPressWithRecognizer:(UILongPressGestureRecognizer *)longPressRecognizer {
	if (longPressRecognizer.state == UIGestureRecognizerStateBegan) {
		self.showsVerticalScrollIndicator = false;
		self.contentOffsetAtStartLongPress = self.contentOffset;
		CGPoint location = [longPressRecognizer locationInView:self];
		self.startIndex = [self.layoutManager
						   characterIndexForPoint:location
						   inTextContainer:self.textContainer
						   fractionOfDistanceBetweenInsertionPoints:nil];
		NSLog(@"%lu", (unsigned long)self.startIndex);
		while (![[self.attributedText.string substringWithRange:NSMakeRange(self.startIndex - 1, 1)] isEqualToString:@" "]) {
			self.startIndex--;
		}
	} else if (longPressRecognizer.state == UIGestureRecognizerStateChanged) {
		self.contentOffset = self.contentOffsetAtStartLongPress;
		[self highlightTextWithCursorLocation:[longPressRecognizer locationInView:self]];
	} else if (longPressRecognizer.state == UIGestureRecognizerStateEnded) {
		[self highlightTextWithCursorLocation:[longPressRecognizer locationInView:self]];
		self.showsVerticalScrollIndicator = true;
	}
}
														 
-(void)highlightTextWithCursorLocation:(CGPoint) location {
	self.endIndex =[self.layoutManager
					characterIndexForPoint:location
					inTextContainer:self.textContainer
					fractionOfDistanceBetweenInsertionPoints:nil];
	while (![[self.attributedText.string substringWithRange:NSMakeRange(self.endIndex, 1)] isEqualToString:@" "]) {
		self.endIndex++;
	}
	
	if (self.endIndex < self.textStorage.length
		&& self.startIndex < self.textStorage.length) {
		self.mutableAttributedText = self.attributedText.mutableCopy;
		[self.mutableAttributedText addAttribute:NSBackgroundColorAttributeName value:[UIColor yellowColor] range:NSMakeRange(self.startIndex, self.endIndex - self.startIndex)];
		self.attributedText = self.mutableAttributedText;
	}
}

@end
